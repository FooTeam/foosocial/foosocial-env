# Environnement de développement du cours FooSocial
## Prérequis
- docker & docker-compose
- php >= 7.4
- composer
- npm

## Installation
Récupérez-le avec git dans un dossier foosocial :

~~~ sh
git clone https://gitlab.com/FooTeam/foosocial/foosocial-env.git foosocial
~~~

Maintenant vous pouvez installer Laravel dans un dossier api avec la commande suivante :

~~~ sh
composer create-project laravel/laravel api
cp api/.env.example api/.env
composer --working-dir=api install --ignore-platform-reqs
php api/artisan key:generate
~~~

De même pour Sapper dans un dossier web :
~~~ sh
npx degit "sveltejs/sapper-template#rollup" web
npm install --prefix web
~~~

## Démarrer l’environnement de développement
~~~ sh
docker-compose up
~~~

## Commandes make
Exécuter les migrations :
~~~ sh
make migrate # docker-compose exec php php artisan migrate
~~~

Remettre à zéro les données et seeder la BDD
~~~ sh
make reset-api
~~~

Suivre les logs de sapper
~~~ sh
make attach-web # docker-compose logs -f node
~~~

Redémarrer sapper
~~~ sh
make restart-web # docker-compose restart node
~~~
