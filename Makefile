#
## Aliases
#

m: migrate
ra: reset-api
rw: restart-web
aw: attach-web
h: help

migrate:		### (Alias=m) Run Laravel migrations
	docker-compose exec php php artisan migrate

reset-api:		### (Alias=ra) Reset database, flush sessions and seed database
	docker-compose exec php php artisan migrate:fresh
	docker-compose exec php php artisan key:generate
	docker-compose exec php php artisan db:seed

restart-web:		### (Alias=rw) Restart the "web" container
	docker-compose restart node
	make attach-web

attach-web:		### (Alias=aw) Attach the logs for the "node" container
	docker-compose logs -f node

help:			### (Current) (Alias=h) See all available commands
	@fgrep -h "###" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/###//'

.PHONY: migrate reset-api reset-web restart-web attach-web help
